package Interface;

public class Car_Demo {
	public static void main(String[] args) {
		Benz ben = new Benz();
		System.out.println("Benz car is " + ben.getColour() + ". Benz car front area is " + ben.getFront_Area(10,20));
		
		Toyota toy = new Toyota();
		System.out.println("Toyota car body made up of "+ toy.getBody() + ". Toyota car seating capacity is " + toy.getSeating_Capacity());
	}
}
