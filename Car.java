package Interface;

public interface Car {
	public int getSeating_Capacity();

	public String getColour();
	
	public String getBody();
	
	public double getFront_Area(double w, double h);
	
	public String getBattery_Type();

	}

