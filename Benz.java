package Interface;

public class Benz implements Car {
	@Override
	public String getColour() {
		return "Black";
	}

	@Override
	public double getFront_Area(double w, double h) {
		return w * h;
	}
	@Override
	public String getBody() {
		return "Coupe";
	}

	@Override
	public int getSeating_Capacity() {
		return 5;
	}

	@Override
	public String getBattery_Type() {
		return "AGM H8 battery";
	}

}
