package Interface;

public class Toyota implements Car {
	@Override
	public String getBody() {
		return "Sedan";
	}

	@Override
	public int getSeating_Capacity() {
		return 6;
	}
	@Override
	public String getColour() {
		return "Grey";
	}

	@Override
	public double getFront_Area(double w, double h) {
		return w * h;
	}

	@Override
	public String getBattery_Type() {
		return "Nickel-Metal Hydride Battery";
	}
}

	